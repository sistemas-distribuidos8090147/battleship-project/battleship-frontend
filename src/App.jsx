import React from "react";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Game from "./Components/Game";

const App = () => {

  return (
    <>
    <BrowserRouter>
      <Routes>
        <Route path="/"></Route>
        <Route path="game" element={<Game />}/>
      </Routes>
    </BrowserRouter>
    </>
  );
};

export default App;
