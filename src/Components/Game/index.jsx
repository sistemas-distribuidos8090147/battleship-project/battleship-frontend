import React from "react";
import Display from "../../Components/Display";
import LogList from "../../Components/Log";
import Heading from "../../Components/Display/Heading";
import useGame from "../../hooks/useGame";

const Game = () => {
    const { myState, opponentState, logState } = useGame();
    return (
        <>
            <Heading />
            <Display {...{ myState, opponentState }} />
            <LogList {...logState} />
        </>
    );
}
export default Game;