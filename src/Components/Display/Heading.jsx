import React from "react";

const Heading = () => {
  return (
    <div className="heading">
      <img src={require('../../res/LogoUEES.png')} width={70} height={90} alt="Logo UEES" />
      <h1> Battleship </h1>
    </div>
  );
};

export default Heading;