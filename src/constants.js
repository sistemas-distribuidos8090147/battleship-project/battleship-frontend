import { getCurrentTime } from "./helpers";

export const columnLabel = "ABCDEFGHIJ";

export const NEW_OPPONENT = "NEW_OPPONENT";
export const NEW_MESSAGE = "NEW_MESSAGE";
export const OPPONENT_LEFT = "OPPONENT_LEFT";
export const NEW_GAME = "NEW_GAME";
export const CLEAR_TILES = "CLEAR_TILES";
export const SELECT_TILE = "SELECT_TILE";
export const CONFIRM_TILES = "CONFIRM_TILES";
export const COMPLETE_SELECTION = "COMPLETE_SELECTION";
export const SET_OPPONENT_SHIPS = "SET_OPPONENT_SHIPS";
export const OPPONENTS_TURN = "OPPONENTS_TURN";
export const SHOT = "SHOT";
export const OPPONENT_SHOT = "OPPONENT_SHOT";
export const END = "END";

export const MISSED = "MISSED";
export const SELECTED = "SELECTED";
export const CONFIRMED = "CONFIRMED";
export const HIT = "HIT";

export const INITIAL_MSG_NO_OPPONENT =
  "Esperando a otro jugador...";
export const INITIAL_MSG_HAVE_OPPONENT =
  "Entrando en modo espectador...";
export const MSG_HAVE_OPPONENT =
  "Modo espectador...";
export const MSG_NO_OPPONENT =
  "El jugador 2 abandono la partida. Esperando a otro jugador...";
export const MSG_INVALID_TILES =
  "Los recuadros deben estar conectados (horizontal o verticalmente).";
export const MSG_ATTACK = "Tu turno.";
export const MSG_DEFEND = "Turno del jugador 2.";
export const MSG_WAITING_FOR_PLAYER = "Esperando a un oponente...";
export const MSG_WIN = "Ganaste! :)";
export const MSG_LOSE = "Perdiste :(";
export const MSG_OPPONENT_PLACING_SHIPS = "El oponente esta ubicando sus barcos...";
export const MSG_ENTER_NEW_GAME = "[NUEVA PARTIDA] Has ingresado a una nueva partida.";

export const ships = [
  { name: "Carrier", numOfTiles: 5 },
  { name: "Battleship", numOfTiles: 4 },
  { name: "Cruiser", numOfTiles: 3 },
  { name: "Submarine", numOfTiles: 3 },
  { name: "Destroyer", numOfTiles: 2 },
];

// initial state used in the hook useGame
export const initialState = () => {
  return {
    gameState: 0,
    shipTilesState: 0,
    messages: [{ time: getCurrentTime(), message: "Bienvenido a Battleship UEES!" }],
    myShips: [],
    myShipsShot: [],
    opponentShips: null,
    chosenTiles: [],
    opponentShipsShot: [],
    opponent: undefined,
    gotInitialOpponent: false,
    haveSendInitialMsg: false,
  };
};

export const backEndUrl = "http://localhost:3001";
