# Build
FROM node:18 AS build

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install

COPY . ./
RUN npm run build

# Run
FROM node:18-alpine
WORKDIR /app
COPY package.json package-lock.json ./
COPY --from=build ./app/build .

RUN npm install -g serve
EXPOSE 3000

CMD ["npm", "start"]
